/*
Note to self:
Old Salt == Volume ID of 1st HDD.
New Salt ?= Phone Hacker 1337 and Reverse Number.
*/

// Set to "done" when complete; is positive while loading; is negative before loading.
var loading = -1;

// Element ID to DOM node table; initialized from idList
var el = {};

// Space delimited list of ID tags that populate the 'el' table upon init() call.
var idList = "passin salt gobutton hexout domain b64out".split(' ');

// Called when finshed loading DOM elements
function init() {
	el.gobutton.addEventListener( "click", doPasshash, true );
}

// Assigns DOM elements when body is fully loaded.
function loader() {
	var body=document.getElementsByTagName("body")[0];
	if ( !body ) {
		loading = setTimeout( loader, 10 );
		return;
	}
	var index;
	for ( index = idList.length; --index >= 0; ){
		var element = document.getElementById( idList[ index ] );
		if ( element ) el[ idList[ index ] ] = element;
		else {
			// Elements are not loaded yet, try again later.
			loading = setTimeout( loader, 20 );
			return;
		}
	}
	clearTimeout( loading );
	loading = "done";
	init();
}
loader();

function doPasshash(){
  var plain = el.passin.value;
  var domain = el.domain.value;
  var salt= el.salt.value;
  var sha1 = new DigestSHA1();
  sha1.add( domain+salt+plain );
  el.hexout.value = sha1.clone().hexDigest();
  el.b64out.value = btoa( sha1.digest() );
}

/*
Creates a new DigestSHA1 object optionally with the given state array.
The state array must have 22 integer elements:
	Elements 0-15:	The current 512 bit block.
	Element 16:		Length of message data that has been added in bytes.
	Elements 17-21:	The result of processing the previous 512 bit block.
*/
function DigestSHA1( state ) {  
  
  // Hexadecimal charset.
  var hexChr = "0123456789abcdef".split('');	

	// Initialization vector.
	var h0 = 0x67452301;
	var h1 = 0xefcdab89;
	var h2 = 0x98badcfe;
	var h3 = 0x10325476;
	var h4 = 0xc3d2e1f0;

  // Total length of message in bytes.
  var byteCount = 0;
  // Current byte within the block.
  var blockByte = 0;
  // Bit fields used in processChunk().
  var block = new Array( 80 );

	// Optionally initialize the internal state from predefined values.
	if ( typeof state != "undefined" ) {
		for ( var i = 0; i < 16; ++i ) block[i] = state[i];
		byteCount = state[16];
		blockByte = byteCount & 0x3f;
		h0 = state[17];
		h1 = state[18];
		h2 = state[19];
		h3 = state[20];
		h4 = state[21];
	} else {
		for ( var i = 0; i < 16; ++i ) block[i] = 0;
	}

	// Return the algorithm name of this digest.
	this.algo = ()=>{ return "SHA-1"; };

	// Add a string of data to the hash.
	this.add = ( str )=>{
		var len = str.length;
		var ch = 0;

		// Process each character of the input string.
		while ( ch < len ) {

			// Fill the first 512 bits of the block.
			while ( (ch < len) && (blockByte < 64) ) {
				var i = blockByte >> 2;					// Index within block array.
				var p = 24 - ((blockByte % 4) << 3);	// bit position within element.

				// Set the bits of the block, and increment counters.
				block[i] |= (str.charCodeAt( ch ) & 255) << p;
				++blockByte; ++ch;
			}

			// Process the 512 bit chunk if enough bits have been added.
			if ( blockByte == 64 ) processChunk();
		}

		byteCount += len;
		return this;
	}

	// Apply padding and length to the internal blocks.
	var finalize = ()=>{
		var len = byteCount << 3;
		this.add( "\x80" );
		while ( blockByte != 56 ) this.add( "\x00" );
		block[14] = (len >>> 16) >>> 16;
		block[15] = len & 0xffffffff;
	}

	// Returns a DigestSHA1 object with the same internal state of this object.
	this.clone = ()=>{
		block[16] = byteCount;
		block[17] = h0;
		block[18] = h1;
		block[19] = h2;
		block[20] = h3;
		block[21] = h4;
		return new DigestSHA1( block );
	}

	// Returns an integer resulting from rolling i to the left n bits.
	function roll( i, n ) {
		return (i >>> (32 - n)) | (i << n);
	}

	// Processes the first 512 bits of the block.
	function processChunk() {

		// Expand the first 512 bits into 2560 bits.
		for ( var i = 16; i < 80; ++i ) {
			block[i] = roll( block[i-3] ^ block[i-8] ^ block[i-14] ^ block[i-16], 1 );
		}

		// Initialize the hash-round variables.
		var a = h0, b = h1, c = h2, d = h3, e = h4, f, g;

		// Process the first 20 rounds.
		for ( var i = 0; i < 20; ++i ) {
			f = (b & c) | ((~b) & d);
			g = roll(a, 5) + f + e + 0x5a827999 + block[i];
			e = d; d = c; c = roll( b, 30 ); b = a; a = g;
		}

		// Process rounds 20-40.
		for ( var i = 20; i < 40; ++i ) {
			f = b ^ c ^ d;
			g = roll(a, 5) + f + e + 0x6ed9eba1 + block[i];
			e = d; d = c; c = roll( b, 30 ); b = a; a = g;
		}

		// Process rounds 40-60.
		for ( var i = 40; i < 60; ++i ) {
			f = (b & c) | (b & d) | (c & d);
			g = roll(a, 5) + f + e + 0x8f1bbcdc + block[i];
			e = d; d = c; c = roll( b, 30 ); b = a; a = g;
		}

		// Process rounds 60-80.
		for ( var i = 60; i < 80; ++i ) {
			f = b ^ c ^ d;
			g = roll(a, 5) + f + e + 0xca62c1d6 + block[i];
			e = d; d = c; c = roll( b, 30 ); b = a; a = g;
		}

		// Set the new hash value.
		h0 += a; h1 += b; h2 += c; h3 += d; h4 += e;

		// Prepare the block for another chunk.
		blockByte = 0;
		for ( var i = 0; i < 16; ++i ) block[i] = 0;
	}

  // Returns a 32 bit hexidecimal string for a given integer.
	function hex( n ){
		var s = '';
		for ( var i = 28; i >= 0; i -= 4 ) s += hexChr[(n >> i) & 15];
		return s;
	}

	// Returns a string contianing 4 ASCII chars corresponding to the 32 bit integer.
	function bin( n ){
		var s = '';
		for ( var i = 24; i >= 0; i -= 8 ) s += String.fromCharCode( (n >> i) & 255 );
		return s;
	}

	// Returns a string with each character containing one byte of the message digest.
	// This function is destructive; After calling the internal state is reset.
	this.digest = ()=>{
		finalize();
		processChunk();
		var s = bin( h0 ) + bin( h1 ) + bin( h2 ) + bin( h3 ) + bin( h4 );
		this.reset();
		return s;
	}

	// Returns a hexidecimal string representation of the message digest.
	// This function is destructive; After calling the internal state is reset.
	this.hexDigest = ()=>{
		finalize();
		processChunk();
		var s = hex( h0 ) + hex( h1 ) + hex( h2 ) + hex( h3 ) + hex( h4 );
		this.reset();
		return s;
	}

	// Prepare the internal state to compute a new digest.
	this.reset = ()=>{
		h0 = 0x67452301;
		h1 = 0xefcdab89;
		h2 = 0x98badcfe;
		h3 = 0x10325476;
		h4 = 0xc3d2e1f0;
		blockByte = 0;
		byteCount = 0;
		for ( var i = 0; i < 16; ++i ) block[i] = 0;
		return this;
	}

} // end DigestSHA1
